*** Life<FredkinCell> 3x3 ***

Generation = 0, Population = 1.
---
0--
---

Generation = 2, Population = 2.
---
0-0
---

Generation = 4, Population = 0.
---
---
---

Generation = 6, Population = 0.
---
---
---

Generation = 8, Population = 0.
---
---
---

Generation = 10, Population = 0.
---
---
---

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 16.
-0---00-
--0--0--
----0--0
---00-0-
-----0--
00------
--0-----
0-0-----

Generation = 1, Population = 22.
0---0-10
---0-1-0
--0-1---
--01-0--
00-0----
11---0--
--10----
--10----

Generation = 2, Population = 23.
-0---02-
0-----01
-01--0-0
0---0-0-
--010---
----0-0-
0---00--
-0--0---

Generation = 3, Population = 28.
--0-01--
-00--1--
0120---1
----10--
001-10--
1-0--0-0
---011--
--101---

Generation = 4, Population = 29.
--1-1-2-
--1--201
1231--0-
------00
-----100
211-0---
0-1-2-00
-021----

Generation = 5, Population = 33.
-02--13-
0----3-2
2-421--1
0001--1-
001--21-
----1---
1--03-11
-132--00

Generation = 6, Population = 26.
-1301-4-
1--0-403
-2-32--2
11----2-
-1---3-0
-11-2--0
--------
-2---0--

Generation = 7, Population = 33.
-2-1215-
-0---51-
23--3-03
22011--0
------11
2-2---01
--1-31-1
0-3-1-0-

Generation = 8, Population = 30.
03-2-2-0
--100-2-
-4--40--
-3---02-
-1-113-2
--302--2
-0---2-2
--4-20--

Generation = 9, Population = 39.
14332-5-
1021----
25----0-
2-01113-
021-2-13
-141-00-
1-1033-3
----3-00

*** Life<FredkinCell> 9x8 ***

Generation = 0, Population = 44.
-0-000--
00--0000
-----0-0
0-000-00
-0000-00
0-00--00
-0-00--0
00-00-00
--00-000

Generation = 3, Population = 39.
-1-3-11-
-02101-1
-022-1-1
001312-3
--10-133
0--1--0-
----1---
-12--0-1
-0--2-30

*** Life<FredkinCell> 3x1 ***

Generation = 0, Population = 2.
0
-
0

Generation = 1, Population = 0.
-
-
-

Generation = 2, Population = 0.
-
-
-

Generation = 3, Population = 0.
-
-
-

Generation = 4, Population = 0.
-
-
-

Generation = 5, Population = 0.
-
-
-

Generation = 6, Population = 0.
-
-
-

Generation = 7, Population = 0.
-
-
-

Generation = 8, Population = 0.
-
-
-

Generation = 9, Population = 0.
-
-
-

*** Life<FredkinCell> 4x8 ***

Generation = 0, Population = 13.
---0---0
000-0-0-
0-0-00--
00------

Generation = 1, Population = 10.
00------
---010--
--1--1--
-1--00--

Generation = 2, Population = 16.
110000--
00-1--0-
----0-0-
0--01-0-

Generation = 3, Population = 17.
-2-1-1--
--021-10
-0-01--0
-101--10

Generation = 4, Population = 17.
1-020--0
-01--02-
01-1-10-
0----02-

Generation = 5, Population = 12.
-2--1---
0--2----
--1--210
1--11--0

Generation = 6, Population = 12.
--0--1--
-----020
-1-1-3--
-1-22--1

Generation = 7, Population = 20.
-2-212-0
-01211-1
02-214--
12--3-2-

Generation = 8, Population = 15.
130-2--1
-1---2-2
-31----0
--0-40-1

Generation = 9, Population = 14.
241---02
--1-----
0--21-1-
--1-51-2

Generation = 10, Population = 17.
3--2-213
--2-1--2
-313-4--
12-2-22-

*** Life<FredkinCell> 10x5 ***

Generation = 0, Population = 29.
-0-0-
0---0
00-0-
--0-0
0-00-
0--00
-0--0
0000-
00000
0-00-

Generation = 3, Population = 29.
-10-0
30-0-
--010
10-10
--110
1-0-0
0--21
011--
-1---
-0-01

Generation = 6, Population = 28.
1--00
---1-
1-32-
413--
--121
2--11
12--1
14--2
-1-04
-30-2

*** Life<FredkinCell> 3x7 ***

Generation = 0, Population = 10.
---0--0
--0-0-0
00--000

Generation = 3, Population = 11.
10--00-
20-0-00
--0---0

Generation = 6, Population = 4.
-------
3-2-3-1
-------

Generation = 9, Population = 0.
-------
-------
-------
